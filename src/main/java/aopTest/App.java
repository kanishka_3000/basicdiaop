package aopTest;

/**
 * Hello world!
 *
 */
import io.*;
import  io.impl.*;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );

        org.springframework.context.ApplicationContext context = new ClassPathXmlApplicationContext("applicationBeans.xml");

        InputManager manager = (InputManager) context.getBean("inputManager");

        manager.process();

    }
}
