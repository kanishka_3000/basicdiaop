package io;

public interface Reader {
    void init(String fileName);
    void readAll(java.util.List<String> items);
}
