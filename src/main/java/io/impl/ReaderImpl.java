package io.impl;

import java.util.List;

public class ReaderImpl implements io.Reader {

    @Override
    public void init(String fileName) {
        System.out.println("ReaderImpl.init");
    }

    @Override
    public void readAll(List<String> items) {
        items.add("BBC");
        items.add("PQR");
    }
}
