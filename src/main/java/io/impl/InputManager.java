package io.impl;

import java.util.List;

public class InputManager {

    io.Reader reader;

    public void setDataItems(List<String> dataItems) {
        this.dataItems = dataItems;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    java.util.List<String> dataItems = new java.util.ArrayList<String>();
    String fileName;

    public InputManager(io.Reader reader){
        this.reader = reader;
    }

    public void process(){
            reader.init(fileName);
            reader.readAll(dataItems);
            for(String item : dataItems){
                System.out.println(item);
            }
    }
}
