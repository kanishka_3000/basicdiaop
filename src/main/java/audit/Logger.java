package audit;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class Logger {

    @Pointcut("execution (* io.impl.*.*(..))")
    public void auditAll(){

    }

    @Before("auditAll()")
    public void beforeCall(){
        System.out.println("Loggin Before");
    }

    @After("auditAll()")
    public void afterCall(){
        System.out.println("Loggin Ater");
    }
}
